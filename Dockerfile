FROM alpine:3

RUN apk add --no-cache bash=~5

COPY files/ /

VOLUME "/opt/tls/" "/opt/out/"

ENTRYPOINT ["/usr/local/sbin/entrypoint.sh"]

CMD [""]