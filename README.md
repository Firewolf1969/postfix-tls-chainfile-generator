# Postfix TLS Chain File Generator

This Docker container creates a single TLS chain file, as specified by
Postfix (http://www.postfix.org/postconf.5.html#smtpd_tls_chain_files)
from the supplied private key, certificate and certificate of the
certification authority (in this order). It is intended as an init container
for Kubernetes.

By default, the source files are expected to be found here:
* `/opt/tls/tls.key`
* `/opt/tls/tls.crt`
* `/opt/tls/ca.crt`

Empty files are ignored.

By default, the chain file is written to `/opt/out/fullchain.pem`
and the source files are copied to the output directory too.

The container can be configured through `CMD` array using the following
parameters:
* `-k <filename>` - alternative location of the key file
* `-c <filename>` - alternative location of the certificate
* `-a <filename>` - alternative location of the CA certificate
* `-o <filename>` - alternative location of the output chain file 
* `-n` - do not copy the source files to the output folder
