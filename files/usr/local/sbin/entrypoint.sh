#!/usr/bin/env bash

set -Eeuo pipefail

DEFAULT_SOURCE_FOLDER="/opt/tls/"
SOURCE_KEYFILE="${DEFAULT_SOURCE_FOLDER}tls.key"
SOURCE_CERTFILE="${DEFAULT_SOURCE_FOLDER}tls.crt"
SOURCE_CAILE="${DEFAULT_SOURCE_FOLDER}ca.crt"
DEFAULT_OUTPUT_FOLDER="/opt/out/"
OUTPUT_CHAINFILE="${DEFAULT_OUTPUT_FOLDER}fullchain.pem"
OUTPUT_COPY_SOURCE="yes"

while getopts ":k:c:a:o:n" o; do
  case "${o}" in
    k)
      SOURCE_KEYFILE="$OPTARG"
      ;;
    c)
      SOURCE_CERTFILE="$OPTARG"
      ;;
    a)
      SOURCE_CAILE="$OPTARG"
      ;;
    o)
      OUTPUT_CHAINFILE="$OPTARG"
      ;;
    n)
      if [[ "$OUTPUT_COPY_SOURCE" == "yes" ]]; then
        OUTPUT_COPY_SOURCE="no"
      else
        OUTPUT_COPY_SOURCE="yes"
      fi
      ;;
    *)
      echo "Invalid command-line arguments."
      echo ""
      echo "Valid arguments are:"
      echo "  -k <filename>   Alternative location of the source TLS key file; defaults to \"$SOURCE_KEYFILE\""
      echo "  -c <filename>   Alternative location of the source TLS cert file; defaults to \"$SOURCE_CERTFILE\""
      echo "  -a <filename>   Alternative location of the source TLS CA cert file; defaults to \"$SOURCE_CAILE\""
      echo "  -o <filename>   Alternative location of the output file; defaults to \"$OUTPUT_CHAINFILE\""
      echo "  -n              Do not copy the source files into the output folder; defaults to \"$OUTPUT_COPY_SOURCE\""

      exit 1
  esac
done

# create the output directory if it does not exist
OUTPUT_DIR=$(dirname -- "$OUTPUT_CHAINFILE")
if [[ ! -d "$OUTPUT_DIR" ]]; then
  echo "Directory \"$OUTPUT_DIR\" does not exist, creating."
  mkdir -p -- "$OUTPUT_DIR"
fi

# create the full chain file
echo "Creating full chain file \"$OUTPUT_CHAINFILE\""
(
  cat -- "$SOURCE_KEYFILE"
  echo ""
  cat -- "$SOURCE_CERTFILE"
  echo ""
  cat -- "$SOURCE_CAILE"
  echo ""
) | grep -v -- "^$" > "$OUTPUT_CHAINFILE"

if [[ "$OUTPUT_COPY_SOURCE" == "yes" ]]; then
  echo "Copying source files to the output directory \"$OUTPUT_DIR\""
  cp -- "$SOURCE_KEYFILE" "$SOURCE_CERTFILE" "$SOURCE_CAILE" "$OUTPUT_DIR"
else
  echo "Not copying source files to the output directory"
fi

echo "Job done"
exit 0